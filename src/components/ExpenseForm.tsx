import React from 'react';
import {Dropdown} from "react-bootstrap";
// import { Container} from "react-bootstrap";

const ExpenseForm =({...props})=>{
    const styles = {
      root: {
        border: "1px solid black",
        backgroundColor: "#F9F9FA",
      },
      label: {
        backgroundColor: "#018DDB",
        padding: "4px 8px",
        height: "2rem",
        color:"#FFF"
      },
      labelInputWithoutBorder: {
        border: "none",
        backgroundColor: "#FFFFFF",        
        padding: "4px 8px",
        height: "2rem",
      },
      labelInputWithBorder: {
        border: "1px solid black",        
        backgroundColor: "#FFFFFF",      
        padding: "4px 8px",
        // minHeight: "2.5rem",
        maxHeight: "3rem",
      },
      labelCheckbox: {
        zoom:"1.75"
      },
      inputIcon:{

        opacity:"0.75",
        padding:"0.5rem 0"
      },
      selectInput:{
        padding:"5px",
      },
      outlineButton:{
        border: "1px solid #018DDB",
        borderRadius:"5px",
        color:"#018DDB",
        backgroundColor:"#FFF",
        padding:"0.3rem 2rem"
      },
      button:{
        border: "1px solid #018DDB",
        borderRadius:"5px",
        color:"#FFF",
        backgroundColor:"#018DDB",
        padding:"0.3rem 2rem"
      }
    };
    return (
        <div className="p-3" style={styles.root}>
          <div className="d-flex flex-row justify-content-start">
            <span style={styles.label}>店課コード</span>
            <input
                className="shadow-none"
                style={{...styles.labelInputWithoutBorder, maxWidth:"5rem"}}
                type="text"
                defaultValue="12345"
            />
            <div className="ml-3 py-1">
              <span>店課名</span>
              <span>４５６７８９０</span>
            </div>
            <span className="ml-3" style={styles.label}>社員コード</span>
            <input
                className="shadow-none"
                style={{...styles.labelInputWithoutBorder, maxWidth:"5rem"}}
                type="text"
                defaultValue="12345"
            />
            <div className="ml-3 py-1">
              <span>社員名</span>
              <span>４５６７８９０</span>
            </div>
          </div>
          <div className="d-flex justify-content-between flex-row">
            <div className="d-flex flex-column px-4 py-3">
              <div className="">
                <span className="">企業コード</span>
              </div>
              <div className="mt-2">
                <div className="d-flex">
                  <input
                      className=""
                      type="text"
                      style={{...styles.labelInputWithBorder, maxWidth:"5rem"}}
                      placeholder="(7桁)"
                  />
                  <span className="p-2 "> {"<<<"} </span>
                  <input
                      className=""
                      type="text"
                      style={{...styles.labelInputWithBorder, maxWidth:"6rem"}}
                      placeholder="(8桁)"
                  />
                  <input
                      className="ml-2"
                      type="text"
                      style={{...styles.labelInputWithBorder, maxWidth:"7rem"}}
                      placeholder="(10桁)"
                  />
                </div>
                <div className="mt-2">
                  <span className="">企業名</span>
                </div>
                <div className="mt-2 d-flex">
                  <input
                      className=""
                      type="text"
                      style={{...styles.labelInputWithBorder, minWidth:"21.5rem"}}
                      placeholder="(前⽅⼀致)"
                  />
                  <input
                      className="mt-1 ml-3"
                      type="checkbox"
                      style={{...styles.labelCheckbox}}
                  />
                  <span className="ml-2 mt-1">部分⼀致</span>
                </div>
                <div className="mt-2">
                  <span className="">企業名カナ</span>
                </div>
                <div className="mt-2 d-flex">
                  <input
                      className=""
                      type="text"
                      style={{...styles.labelInputWithBorder, minWidth:"21.5rem"}}
                      placeholder="(前⽅⼀致)"
                  />
                  <input
                      className="mt-1 ml-3"
                      type="checkbox"
                      style={{...styles.labelCheckbox}}
                  />
                  <span className="ml-2 mt-1">部分⼀致</span>
                </div>
                <div className="mt-2">
                  <span className="">電話番号</span>
                </div>
                <div className="mt-2 d-flex">
                  <input
                      className=""
                      type="text"
                      style={{...styles.labelInputWithBorder, maxWidth:"10rem"}}
                      defaultValue="0312345678"
                  />
                </div>
                <div className="mt-2 ">
                  <span className="">住所</span>
                </div>
                <div className="mt-2 d-flex">
                  <div style={{border:"1px solid black"}} className="">
                    <Dropdown>
                      <Dropdown.Toggle variant="" id="dropdown-basic">
                        <span>都道府県</span>
                      </Dropdown.Toggle>

                      <Dropdown.Menu>
                        <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                        <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                        <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                  <input
                      className="ml-2"
                      type="text"
                      style={{...styles.labelInputWithBorder, minWidth:"15rem"}}
                      placeholder="(前⽅⼀致)"

                  />
                  <input
                      className="mt-1 ml-3"
                      type="checkbox"
                      style={{...styles.labelCheckbox}}
                  />
                  <span className="ml-2 mt-1">部分⼀致</span>
                </div>
              </div>
            </div>
            <div className="d-flex flex-column px-4 py-3">
              <div className="">
                <span className="">経費⽐較番号</span>
              </div>
              <div className="mt-2">
                <div className="d-flex">
                  <input
                      className=""
                      type="text"
                      style={{...styles.labelInputWithBorder, maxWidth:"6rem"}}
                      defaultValue="1234567"
                  />
                  <span className="p-2 font-weight-bold"> {"-"} </span>
                  <input
                      className=""
                      type="text"
                      style={{...styles.labelInputWithBorder, maxWidth:"8rem"}}
                      defaultValue="20211231"
                  />
                  <span className="p-2 font-weight-bold"> {"-"} </span>
                  <input
                      className="ml-2"
                      type="text"
                      style={{...styles.labelInputWithBorder, maxWidth:"4rem"}}
                      defaultValue="001"
                  />
                </div>
              </div>
              <div className="mt-2 ">
                <span className="">経費件名</span>
              </div>
              <div className="mt-2 d-flex">
                <input
                    className=""
                    type="text"
                    style={{...styles.labelInputWithBorder, minWidth:"22.5rem"}}
                    placeholder="(前⽅⼀致)"
                />
                <input
                    className="mt-1 ml-3"
                    type="checkbox"
                    style={{...styles.labelCheckbox}}
                />
                <span className="mt-1 ml-2">部分⼀致</span>
              </div>
              <div className="mt-2 ">
                <span className="">最終更新期間指定</span>
              </div>
              <div className="mt-2 d-flex">
                <input
                    className=""
                    type="text"
                    style={{...styles.labelInputWithBorder, maxWidth:"10.5rem"}}
                    placeholder="⼊⼒または選択"
                />
                <span style={styles.inputIcon} className="ml-n4"><i className="fa fa-calendar"></i></span>
                <span className="px-4 py-2 font-weight-bold"> {"~"} </span>
                <input
                    className=""
                    type="text"
                    style={{...styles.labelInputWithBorder, maxWidth:"10.5rem"}}
                    placeholder="⼊⼒または選択"
                />
                <span style={styles.inputIcon} className="ml-n4"><i className="fa fa-calendar"></i></span>
              </div>
              <div className="mt-2 ">
                <span className="">台数指定</span>
              </div>
              <div className="mt-2 d-flex">
                <input
                    className=""
                    type="text"
                    style={{...styles.labelInputWithBorder, maxWidth:"5rem"}}
                    defaultValue="1234"

                />
                <span className="px-4 py-2 font-weight-bold"> {"~"} </span>
                <input
                    className=""
                    type="text"
                    style={{...styles.labelInputWithBorder, maxWidth:"5rem"}}
                    defaultValue="1234"
                />
              </div>
            </div>
          </div>
          <div className="d-flex justify-content-center">
            <button className="" style={styles.outlineButton} type="button">クリア</button>
            <button className="ml-3" style={styles.button}  type="button">検索</button>
          </div>
        </div>
    );
}
export default ExpenseForm;