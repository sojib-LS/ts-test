import React, {CSSProperties}from 'react';
import {Dropdown} from "react-bootstrap";
import "./Modal.css";

const ExpenseForm =({...props})=>{
    const styles = {
      root: {
        border: "1px solid black",
        backgroundColor: "#F9F9FA",
      },
      label: {
        backgroundColor: "#018DDB",
        padding: "4px 8px",
        height: "2rem",
        color:"#FFF"
      },
      labelInputWithoutBorder: {
        border: "none",
        backgroundColor: "#FFFFFF",        
        padding: "4px 8px",
        height: "2rem",
      },
      labelInputWithBorder: {
        border: "1px solid black",        
        backgroundColor: "#FFFFFF",      
        padding: "4px 8px",
        // minHeight: "2.5rem",
        maxHeight: "3rem",
      },
      labelCheckbox: {
        zoom:"1.75"
      },
      inputIcon:{
        opacity:"0.75",
        padding:"0.5rem 0"
      },
      selectInput:{
        padding:"5px",
      },
      outlineButton:{
        border: "1px solid #018DDB",
        borderRadius:"5px",
        color:"#018DDB",
        backgroundColor:"#FFF",
        padding:"0.3rem 2rem"
      },
      button:{
        border: "1px solid #018DDB",
        borderRadius:"5px",
        color:"#FFF",
        backgroundColor:"#018DDB",
        padding:"0.3rem 2rem"
      },
      iconButton:{
        color:"#000"
      },
      iconButtonActive:{
       color:"#018DDB"
      },
    };
    return (
        <div className="p-3" style={styles.root}>
          <div className="d-flex justify-content-end">
            <div style={{border:"1px solid black"}} className="w-full h-full ml-3">
              <Dropdown>
                <Dropdown.Toggle variant="" id="dropdown-basic">
                  <span>10件表⽰</span>
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                  <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                  <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
          <div className="table-div-height-scroll mt-2">
            <table id="hospitalTable" className="table border my-custom-table">
              <thead id="hospital-list-head">
              <tr id="hospital-list-head-row">
                <th>
                  <div>企業コード 7桁 <span className="triangle-icon-button">
                    <i className="fa fa-caret-down"/>
                  </span></div>
                </th>
                <th>８桁／10桁</th>
                <th>
                  <div>企業名 <span className="triangle-icon-button">
                    <i className="fa fa-caret-down"/>
                  </span></div>
                </th>
                <th>
                  <div>部署名 <span className="triangle-icon-button">
                    <i className="fa fa-caret-down"/>
                  </span></div>
                </th>
                <th>住所</th>
                <th>
                  <div>企業コード 7桁 <span className="triangle-icon-button">
                    <i className="fa fa-caret-down"/>
                  </span></div>
                </th>
                <th>８桁／10桁</th>
                <th>
                  <div>企業名 <span className="triangle-icon-button">
                    <i className="fa fa-caret-down"/>
                  </span></div>
                </th>
                <th>
                  <div>部署名 <span className="triangle-icon-button">
                    <i className="fa fa-caret-down"/>
                  </span></div>
                </th>
                <th>住所</th>

              </tr>
              </thead>
              <tbody id="hospital-list">
              <tr>
                <td rowSpan={2} className="">1234567</td>
                <td className="">12345678</td>
                <td rowSpan={2} className="">株式会社　AIUEO</td>
                <td rowSpan={2} className="">第1事業部</td>
                <td rowSpan={2} className="">東京都港区港南⼀丁⽬6番31号　品</td>
                <td rowSpan={2} className="">1234567</td>
                <td className="">12345678</td>
                <td rowSpan={2} className="">株式会社　AIUEO</td>
                <td rowSpan={2} className="">第1事業部</td>
                <td rowSpan={2} className="">東京都港区港南⼀丁⽬6番31号　品</td>

              </tr>
              <tr>
                <td>1234567890</td>
                <td>1234567890</td>
              </tr>
              <tr>
                <td rowSpan={2} className="">0000002</td>
                <td className="">11223344</td>
                <td rowSpan={2} className="">株式会社　AIUEO　静岡営業祖　APL開発センター</td>
                <td rowSpan={2} className="">第1事業部</td>
                <td rowSpan={2} className="">静岡県静岡市葵区七間町8-20　毎</td>
                <td rowSpan={2} className="">0000002</td>
                <td className="">11223344</td>
                <td rowSpan={2} className="">株式会社　AIUEO　静岡営業祖　APL開発センター</td>
                <td rowSpan={2} className="">第1事業部</td>
                <td rowSpan={2} className="">静岡県静岡市葵区七間町8-20　毎</td>

              </tr>
              <tr>
                <td>1000000001</td>
                <td>1000000001</td>
              </tr>
              </tbody>
            </table>
          </div>
          <div className="d-flex justify-content-center mt-2">
            <div style={styles.iconButton} className="p-1"><i className="fa fa-step-backward"></i></div>
            <div style={{...styles.iconButton, fontSize:"1.3rem"}} className="ml-2"><i className="fa fa-caret-left"></i></div>
            <div className="px-4">
              <input style={{...styles.labelInputWithBorder, maxWidth:"2rem"}} type="text" defaultValue="1"/>
              <span className="ml-2">/ {"10"}</span>
            </div>
            <div style={styles.iconButtonActive} className="p-1 ml-2"><i className="fa fa-step-forward"></i></div>
            <div style={{...styles.iconButtonActive, fontSize:"1.3rem"}} className="ml-2"><i className="fa fa-caret-right"></i></div>

          </div>
          <div className="d-flex justify-content-end">
            <button className="ml-3" style={styles.button}  type="button">経費⽐較􀀀表を削除</button>
          </div>
        </div>
    );
}
export default ExpenseForm;