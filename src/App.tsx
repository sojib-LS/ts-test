import React from 'react';
import './App.css';
import ExpenseForm from './components/ExpenseForm';
import ExpenseTable from "./components/ExpenseTable";

function App() {
  return (
    <div className="container-fluid p-5">
     <div className="p-1">
       <ExpenseForm/>
         <ExpenseTable/>
     </div>
    </div>
  );
}

export default App;
